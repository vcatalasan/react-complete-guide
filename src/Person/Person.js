import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classes from './Person.css';
import Aux from '../Utils/Aux';
import withClass from '../Utils/withClass';

class Person extends Component {

    componentDidMount() {
       this.props.position == 0 && this.inputElement.focus();
    }

    render() {
        const {name, age, swap, change, add, remove} = this.props;
        return (
            <Aux>
                <h1>{name}</h1>
                <p onClick={swap}>Your age is {age}</p>
                <input
                    ref={(inp) => this.inputElement = inp}
                    type="text"
                    onChange={change}
                    value={name}/>
                <button onClick={add}>+</button>
                <button onClick={remove}>-</button>
            </Aux>
        );
    };
}

Person.propTypes = {
    name: PropTypes.string,
    age: PropTypes.number,
    swap: PropTypes.func,
    change: PropTypes.func,
    add: PropTypes.func,
    remove: PropTypes.func
}

export default withClass(Person, classes.Person);