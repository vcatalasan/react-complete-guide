const redux = require('redux');
const createStore = redux.createStore;

// Initialize state
const initialState = {
    counter: 0
};

// Reducer
const rootReducer = (state = initialState, action) => {
    if (action.type === 'INC_COUNTER') {
        //never mutate any state directly
        //make a copy of the state and return the new copy after making your changes
        return {
            ...state,   //copy the original state
            counter: state.counter+1   //override the state we want to change
        }
    }
    if (action.type === 'ADD_COUNTER') {
        //never mutate any state directly
        //make a copy of the state and return the new copy after making your changes
        return {
            ...state,   //copy the original state
            counter: state.counter+action.value   //override the state we want to change
        }
    }
    return state;
};

// Store
const store = createStore(rootReducer);
console.log(store.getState());

// Subscription
store.subscribe(() => {
    console.log('[Subscription]', store.getState());
});

// Dispatching Action
store.dispatch({type: 'INC_COUNTER'});
store.dispatch({type: 'ADD_COUNTER', value: 10});
console.log(store.getState());

