import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Nav from './Nav/Nav';
import classes from './App.css';
import Person from './Person/Person';
import Counter from './Counter/Counter';
import Aux from './Utils/Aux';
import withClass from './Utils/withClass';

class App extends Component {
  state = {
    persons: [
      { id: 'ad11', name: 'Val', age: 58 },
      { id: 'df11', name: 'Monette', age: 26 }
    ],
    showPersons: false,
    toggleClicked: 0
  };

  toggleClicked = (event) => {
    this.setState((prevState, props) => {
        return {
          showPersons: !prevState.showPersons,
          toggleClicked: prevState.toggleClicked+1
        }
    });
  };

  swapAgeHandler = (event, id) => {
    //make a copy of the current state of persons
    const persons = [ ...this.state.persons ];
    //find the person selected by id
    const personIndex = persons.findIndex(
      person => person.id === id
    );
    //get next person from list
    const personNext = personIndex + 1 >= persons.length ? 0 : personIndex + 1;
    //swap selected person's age with the next person
    persons[personNext].age = [
      persons[personIndex].age,
      persons[personIndex].age = persons[personNext].age
    ][0]; 
    //Update state of persons
    this.setState({
      persons: persons
    });
  };

  changeNameHandler = (event, id) => {
    //make a copy of current state of persons
    const persons = [ ...this.state.persons ];
    //find the person selected by id
    const personIndex = persons.findIndex(
      person => person.id === id
    );
    //Set new name
    persons[personIndex].name = event.target.value;
    //Update state of persons
    this.setState(
      { persons: persons }
    )
  };

  addPersonHandler = (event, id) => {
    //make a copy of current state of persons
    const persons = [ ...this.state.persons ];
    //find the person selected by id
    const personIndex = persons.findIndex(
      person => person.id === id
    );
    //add a new person in the list after the selected person
    const num = Math.round(Math.random()*100);
    persons.splice(personIndex+1, 0, {
      id: 'A'+num,
      name: 'What\'s your name?',
      age: num
    });
    //Update state of persons
    this.setState(
      { persons: persons }
    )
  };

  removePersonHandler = (event, id) => {
    //make a copy of current state of persons
    const persons = [ ...this.state.persons ];
    //find the person selected by id
    const personIndex = persons.findIndex(
      person => person.id === id
    );
    //remove person from the list
    persons.splice(personIndex, 1);
    //Update state of persons
    this.setState(
      { persons: persons }
    )
  };

  personList = () => {
    return this.state.persons.map(
      (p, index) => <Person
          key={p.id}
          position={index}
          name={p.name} age={p.age} 
          swap={event => this.swapAgeHandler(event, p.id)}
          change={event => this.changeNameHandler(event, p.id)}  
          add={event => this.addPersonHandler(event, p.id)}
          remove={event => this.removePersonHandler(event, p.id)}
          />
    );
  };

  render() {
  
    let assignedClasses = [];
    if (this.state.persons.length <= 2) {
      assignedClasses.push(classes.red);
    }

    if (this.state.persons.length <= 1) {
      assignedClasses.push(classes.bold);
    }

    return (
        <Router>
          <div>
            <Nav />
            <Route path="/" exact component={Counter} />
            <Route path="/Person" exact render={() => (
                <Aux>
                  <h1 className={assignedClasses.join(' ')}>Hi, I'm a React App</h1>
                  <p>This is really working!</p>
                  <button onClick={this.toggleClicked} className={classes.Red}>Toggle Persons</button>
                  {this.state.showPersons ? this.personList() : ''}
                </Aux>
                )} />
          </div>
        </Router>
    );
  }
}

export default withClass(App, classes.App);
