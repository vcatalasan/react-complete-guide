import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Row, Col, Container, ListGroup, ListGroupItem } from 'mdbreact';
import * as actionTypes from '../store/action';

class Counter extends Component {

  /*
  the following internal state and functions has been replaced with redux
  */
  state = {
      counter: 0
  };

  increment = () => {
    this.setState((prev, props) => (
        {counter: prev.counter + 1}
        )
    );
  };

  decrement = () => {
      this.setState((prev, props) => (
              {counter: prev.counter - 1}
          )
      );
  };

  add = () => {
      this.setState((prev, props) => (
              {count: prev.count + props.add}
          )
      );
  };

  subtract = () => {
      this.setState((prev, props) => (
              {counter: prev.counter - props.substract}
          )
      );
  };

  render() {
      return (
          <Container>
              <Row>
                  <Col>
                      <Button onClick={this.props.onIncrementCounter}>Increment</Button>
                      <Button onClick={this.props.onDecrementCounter}>Decrement</Button>
                      <Button onClick={this.props.onAddCounter}>Add 5</Button>
                      <Button onClick={this.props.onSubtractCounter}>Subtract 5</Button>
                  </Col>
                  <Col>
                      <h1>Current Counter: {this.props.counter}</h1>
                      <Button onClick={() => this.props.onStoreResult(this.props.counter)}>Store Result</Button>
                  </Col>
              </Row>
              <Row>
                  <ListGroup>
                      {this.props.results.map((item) => (
                          <ListGroupItem key={item.id} onClick={() => this.props.onDeleteResult(item.id)}>{item.value}</ListGroupItem>
                      ))}
                  </ListGroup>
              </Row>
          </Container>
      );
  }
}

// #6 - map redux store states to this component's props
const mapStateToProps = state => {
    return {
        counter: state.cr.counter,
        results: state.rr.results
    }
};

// #7 - map redux actions to this component's props
const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch({ type: actionTypes.INCREMENT}),
        onDecrementCounter: () => dispatch({ type: actionTypes.DECREMENT}),
        onAddCounter: () => dispatch({ type: actionTypes.ADD, value: 5}),
        onSubtractCounter: () => dispatch({ type: actionTypes.SUBTRACT, value: 5}),
        onStoreResult: (result) => dispatch({ type: actionTypes.STORE_RESULT, result: result}),
        onDeleteResult: (id) => dispatch({ type: actionTypes.DELETE_RESULT, id: id})
    }
};

// #8 - connect redux's state and actions to this component's props
export default connect(mapStateToProps, mapDispatchToProps)(Counter);