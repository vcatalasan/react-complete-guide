import React from 'react';
import ReactDOM from 'react-dom';

// Topic: Using redux with react to manage multiple reducers
// #1 - import redux and react-redux package
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';

// #2 - import multiple reducers
import counterReducer from './store/reducers/counter';
import resultsReducer from './store/reducers/results';

import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './index.css';

import App from './App';
import registerServiceWorker from './registerServiceWorker';

// #3 - create a root reducer combining all imported reducers
const rootReducer = combineReducers({
    cr: counterReducer,
    rr: resultsReducer
});

// #4 - create a redux store using the root reducer
const store = createStore(rootReducer);

// #5 - Wrap your <App> component with the <Provider> HOC supplied by react-redux passing it the redux store
ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
